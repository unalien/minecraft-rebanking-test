/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import MainNav from './src/navigation/MainNav';
import Store from './src/store/Store';
import {Provider} from 'react-redux';
const App: () => React$Node = () => {
  return (
    <Provider store={Store}>
      <StatusBar barStyle="dark-content" />
      <MainNav />
    </Provider>
  );
};

export default App;
