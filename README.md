
Abrir una terminal en él root del proyecto y hacer npm install

Las dependencias estan en él package.json pero en él caso de errores los siguientes comandos pueden ser utiles:

rm -rf node_modules 
yarn install o npm install
react-native install react-navigation
react-native install react-navigation-stack
react-native install redux
react-native install react-redux
react-native install react-thunk

Requisito para probar la App
Tener un dispositivo en modo debugger o un simulador
Correr react-native run-ios o run-android

Generar compilados

Android

Comandos usados:
Borra el build folder
./gradlew clean
Genera el apk
/gradlew assemble(Release | Debug)

Para release
Generar una Key en android/app
keytool -genkey -v -keystore mykeystore.keystore -alias mykeyalias -keyalg RSA -keysize 2048 -validity 10

En android/app/build.gradle agregar
android { 
signingConfigs { 
release { 
storeFile file(MYAPP_RELEASE_STORE_FILE) 
storePassword MYAPP_RELEASE_STORE_PASSWORD 
keyAlias MYAPP_RELEASE_KEY_ALIAS 
keyPassword MYAPP_RELEASE_KEY_PASSWORD 
}
} 
buildTypes { 
release { 
signingConfig signingConfigs.release 
}
}
}

En la carpeta android abrir gradle.properties y agregar
TEST_RELEASE_STORE_FILE=XXXXX.keystore
TEST_RELEASE_KEY_ALIAS=XXXXX
TEST_RELEASE_STORE_PASSWORD=XXXXX
TEST_RELEASE_KEY_PASSWORD=XXXXX

Estando en el root poner en la terminal:
cd android
./gradlew clean && ./gradlew assembleRelease

El compilado va a aparecer en android/app/build/outputs/apk/app-release.apk

Para debug es lo mismo pero usando /gradlew assembleDebug usando la key.debug y el apk se llamará app-debug.apk
