import {connect} from 'react-redux';
import Item from '../components/Item';
import {doFetchItemList} from '../actions/itemListAction';
import withFavoriteHOC from './hoc/withFavoriteHOC';

const mapStateToProps = (state, ownProps) => ({
  itemList: state.items.list,
  error: state.items.error,
  loading: state.items.loading,
  favList: ownProps.favList,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  fetchItemList: url => dispatch(doFetchItemList(url)),
  addToFav: ownProps.addToFav,
  removeFromFav: ownProps.removeFromFav,
});

//Encapsulo al container con el HOC de favoritos
//nested connect esta soportado por redux
export default withFavoriteHOC(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Item),
);
