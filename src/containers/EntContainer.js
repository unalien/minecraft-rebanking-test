import {connect} from 'react-redux';
import Entity from '../components/Entity';

import withFavoriteHOC from './hoc/withFavoriteHOC';

import {doFetchEntityList} from '../actions/entityAction';

const mapStateToProps = (state, ownProps) => ({
  entList: state.entities.list,
  error: state.entities.error,
  loading: state.entities.loading,
  favList: ownProps.favList,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  fetchEntList: url => dispatch(doFetchEntityList(url)),
  addToFav: ownProps.addToFav,
  removeFromFav: ownProps.removeFromFav,
});

//Encapsulo al container con el HOC de favoritos
//nested connect esta soportado por redux
export default withFavoriteHOC(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Entity),
);
