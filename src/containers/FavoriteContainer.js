import {connect} from 'react-redux';
import {doAddToFav, doRemoveFromFav} from '../actions/favAction';
import Favorite from '../components/Favorite';

const mapStateToProps = state => ({
  favList: state.favorite.favList,
});

const mapDispatchToProps = dispatch => ({
  doAddToFav: name => dispatch(doAddToFav(name)),
  removeFromFav: name => dispatch(doRemoveFromFav(name)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Favorite);
