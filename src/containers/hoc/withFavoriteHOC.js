import React, {Component} from 'react';

import {connect} from 'react-redux';
import {doAddToFav, doRemoveFromFav} from '../../actions/favAction';

const withFavoriteHOC = WrappedComponent => {
  class WithFavoriteHOC extends Component {
    constructor(props) {
      super(props);
    }
    state = {list: []};

    render() {
      return (
        <WrappedComponent
          favList={this.props.favList}
          addToFav={this.props.addToFav}
          removeFromFav={this.props.removeFromFav}
        />
      );
    }
  }
  const mapStateToProps = state => ({
    favList: state.favorite.favList,
  });

  const mapDispatchToProps = dispatch => ({
    addToFav: row => dispatch(doAddToFav(row)),
    removeFromFav: name => dispatch(doRemoveFromFav(name)),
  });
  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(WithFavoriteHOC);
};

export default withFavoriteHOC;
