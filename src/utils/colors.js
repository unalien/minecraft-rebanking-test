export default Colors = {
  lightPink: '#FFC0CB',
  pink: '#FFB6C1',
  aquamarine: '#66CDAA',
  orange: '#FF8C00',
};
