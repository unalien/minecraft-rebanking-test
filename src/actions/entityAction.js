export const FETCH_ENTITIES_BEGIN = 'FETCH_ENTITIES_BEGIN';
export const FETCH_ENTITIES_SUCCESS = 'FETCH_ENTITIES_SUCCESS';
export const FETCH_ENTITIES_FAILURE = 'FETCH_ENTITIES_FAILURE';

export const fetchEntitiesBegin = () => ({
  type: FETCH_ENTITIES_BEGIN,
});

export const fetchEntitiesSuccess = entities => ({
  type: FETCH_ENTITIES_SUCCESS,
  payload: entities,
});

export const fetchEntitiesFailure = error => ({
  type: FETCH_ENTITIES_FAILURE,
  payload: {error},
});

export const doFetchEntityList = url => {
  return dispatch => {
    dispatch(fetchEntitiesBegin());
    fetch(url)
      .then(response => {
        return response.json();
      })
      .then(json => {
        console.log(json);
        dispatch(fetchEntitiesSuccess(json));
        return json;
      })
      .catch(error => dispatch(fetchEntitiesFailure(error)));
  };
};
