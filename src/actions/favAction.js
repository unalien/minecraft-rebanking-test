export const ADD_TO_FAV = 'ADD_TO_FAV';
export const REMOVE_FROM_FAV = 'REMOVE_FROM_FAV';

export function doAddToFav(row) {
  return {type: ADD_TO_FAV, payload: row};
}

export function doRemoveFromFav(name) {
  return {type: REMOVE_FROM_FAV, payload: name};
}
