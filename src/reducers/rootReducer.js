import {combineReducers} from 'redux';
import favoriteReducer from './favoriteReducer';
import entityListReducer from './entityListReducer';
import itemListReducer from './itemListReducer';

export default rootReducer = combineReducers({
  favorite: favoriteReducer,
  entities: entityListReducer,
  items: itemListReducer,
});
