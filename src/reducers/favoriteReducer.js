import {ADD_TO_FAV, REMOVE_FROM_FAV} from '../actions/favAction';

const initialState = {
  favList: [],
};
export default favoriteReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case ADD_TO_FAV:
      return {...state, favList: state.favList.concat(action.payload)};

    case REMOVE_FROM_FAV:
      return {
        ...state,
        favList: state.favList.filter(row => {
          return action.payload.name !== row.name;
        }),
      };
  }
  return state;
};
