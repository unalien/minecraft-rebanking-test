import {
  FETCH_ENTITIES_BEGIN,
  FETCH_ENTITIES_SUCCESS,
  FETCH_ENTITIES_FAILURE,
} from '../actions/entityAction';

const initialState = {
  list: [],
  loading: false,
  error: undefined,
};
export default entityListReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case FETCH_ENTITIES_BEGIN:
      return {
        ...state,
        loading: true,
        error: undefined,
      };

    case FETCH_ENTITIES_SUCCESS:
      return {
        ...state,
        loading: false,
        list: state.list.concat(...action.payload),
      };

    case FETCH_ENTITIES_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        list: [],
      };

    default:
      return state;
  }
};
