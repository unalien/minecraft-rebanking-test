import {
  FETCH_ITEMS_BEGIN,
  FETCH_ITEMS_SUCCESS,
  FETCH_ITEMS_FAILURE,
} from '../actions/itemListAction';

const initialState = {
  list: [],
  loading: false,
  error: undefined,
};
export default itemListReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case FETCH_ITEMS_BEGIN:
      return {
        ...state,
        loading: true,
        error: undefined,
      };

    case FETCH_ITEMS_SUCCESS:
      return {
        ...state,
        loading: false,
        list: state.list.concat(...action.payload),
      };

    case FETCH_ITEMS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        list: [],
      };

    default:
      return state;
  }
};
