import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import MenuScreen from '../screens/MenuScreen';
import LoginScreen from '../screens/LoginScreen';
import ItemScreen from '../screens/ItemScreen';
import EntitieScreen from '../screens/EntitieScreen';
import FavoriteScreen from '../screens/FavoriteScreen';

const MainNav = createStackNavigator({
  Login: {
    screen: LoginScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  Main: {
    screen: MenuScreen,
    navigationOptions: ({navigation}) => ({
      headerShown: false,
    }),
  },
  Items: {
    screen: ItemScreen,
    navigationOptions: ({navigation}) => ({
      title: 'BLOQUES',
    }),
  },
  Ent: {
    screen: EntitieScreen,
    navigationOptions: ({navigation}) => ({
      title: 'COSAS',
    }),
  },
  Fav: {
    screen: FavoriteScreen,
    navigationOptions: ({navigation}) => ({
      title: 'FAVORITO',
    }),
  },
});

export default createAppContainer(MainNav);
