import React, {Component} from 'react';
import {View, StyleSheet, FlatList, TextInput, Image, Text} from 'react-native';

import {itemsUrl} from '../data/const/urlConst';

import RenderItem from '../components/RenderItem';
import Colors from '../utils/colors';

import LoadingAndError from './LoadingAndError';

class Item extends Component {
  componentDidMount() {
    console.log(this.props);
    //Si ya esta cargado en el store no lo vuelvo a llamar
    if (this.props.itemList.length === 0) {
      this.props.fetchItemList(itemsUrl);
    }
  }

  state = {searchValue: ''};

  //Checkeo el estado del searchValue es vacio
  // para volver a cargar la de las props
  static getDerivedStateFromProps(nextProps, prevState) {
    if (
      nextProps.itemList.length > 0 &&
      (prevState.searchValue === undefined || prevState.searchValue === '')
    ) {
      return {list: [...nextProps.itemList]};
    } else {
      return {};
    }
  }
  // Les debo el container de error y loading
  render() {
    if (this.props.loading) {
      return <LoadingAndError message={'Cargando'} />;
    } else if (this.props.error !== undefined) {
      return (
        <LoadingAndError message={'Hubo un error al recuperar los items'} />
      );
    } else {
      return (
        <View style={styles.container}>
          <TextInput
            placeholder="Search an item..."
            style={styles.inputText}
            onChangeText={text => {
              //Genero una nueva lista filtrada
              const tmpList = this.props.itemList.filter(item => {
                return item.name.toLowerCase().includes(text.toLowerCase());
              });
              this.setState({list: tmpList, searchValue: text});
            }}
          />
          <FlatList
            data={this.state.list}
            numColumns={1}
            keyExtractor={item => item.name.concat(item.text_type)}
            renderItem={item => {
              return RenderItem(
                item,
                this.props.addToFav,
                this.props.removeFromFav,
                this.props.favList,
              );
            }}
            maxToRenderPerBatch={10}
            initialNumToRender={20}
          />
        </View>
      );
    }
  }
}

//Utilizo StyleSheet en vez de CSS para evitar que
//el style pase por el bridge cada render
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  inputText: {
    borderColor: Colors.pink,
    borderWidth: 0.5,
    padding: '2%',
    margin: '5%',
  },
  icon: {
    resizeMode: 'contain',
    height: '20%',
    width: '20%',
  },
});

export default Item;
