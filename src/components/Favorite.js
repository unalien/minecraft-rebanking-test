import React, {Component} from 'react';
import {View, Image, FlatList, Text, StyleSheet} from 'react-native';

import RenderItem from '../components/RenderItem';
import Colors from '../utils/colors';

class Favorite extends Component {
  componentDidMount() {
    console.log(this.props);
  }

  render() {
    if (this.props.favList.length === 0) {
      return (
        <View style={styles.notfound}>
          <Image
            resizeMode="center"
            style={styles.image}
            source={require('../../assets/llama.png')}
          />
          <Text style={styles.text}>No agregaste nada a favorito</Text>
        </View>
      );
    } else {
      return (
        <FlatList
          data={this.props.favList}
          numColumns={1}
          keyExtractor={item => item.name.concat(item.type_text)}
          renderItem={item => {
            return RenderItem(
              item,
              undefined,
              this.props.removeFromFav,
              this.props.favList,
            );
          }}
        />
      );
    }
  }
}
const styles = StyleSheet.create({
  image: {width: '40%', height: '40%'},
  notfound: {flex: 1, alignItems: 'center', justifyContent: 'center'},
  text: {fontSize: 25, color: Colors.pink, fontWeight: 'bold'},
});
export default Favorite;
