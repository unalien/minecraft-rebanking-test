import React, {Component} from 'react';
import {FlatList} from 'react-native';

import {entityUrl} from '../data/const/urlConst';
import RenderItem from '../components/RenderItem';

import LoadingAndError from './LoadingAndError';
class Monster extends Component {
  componentDidMount() {
    //Si ya esta cargado en el store no lo vuelvo a llamar
    console.log(entityUrl);
    if (this.props.entList.length === 0) {
      this.props.fetchEntList(entityUrl);
    }
  }

  //Este no lleva search, para diferenciar un poco

  render() {
    if (this.props.loading) {
      return <LoadingAndError message={'Cargando'} />;
    } else if (this.props.error !== undefined) {
      return (
        <LoadingAndError message={'Hubo un error al recuperar los entities'} />
      );
    } else {
      return (
        <FlatList
          data={this.props.entList}
          numColumns={1}
          keyExtractor={item => item.name}
          renderItem={item => {
            return RenderItem(
              item,
              this.props.addToFav,
              this.props.removeFromFav,
              this.props.favList,
            );
          }}
        />
      );
    }
  }
}

export default Monster;
