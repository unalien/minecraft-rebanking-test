import React from 'react';
import {getItemImage, getEntityImage} from '../data/const/Images';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import Colors from '../utils/colors';

export default renderItem = (itemData, addToFav, removeFromFav, favList) => {
  const type =
    itemData.item.meta === undefined || itemData.item.meta === ''
      ? itemData.item.type
      : itemData.item.type + '' + itemData.item.meta;

  const existe = favList.find(itemAdded => {
    return itemData.item.name === itemAdded.name;
  });

  return (
    <View style={styles.container}>
      {itemData.item.meta === undefined ? (
        <Image resizeMode="contain" source={getEntityImage(type)} />
      ) : (
        <Image resizeMode="contain" source={getItemImage(type)} />
      )}

      <Text style={styles.text}>{itemData.item.name}</Text>
      <View style={styles.imageContainer}>
        {addToFav !== undefined &&
        (existe === false || existe === undefined) ? (
          <TouchableOpacity
            onPress={() => {
              addToFav({
                name: itemData.item.name,
                type: itemData.item.type,
                type_text: itemData.type_text,
                meta: itemData.item.meta,
              });
            }}>
            <Image
              resizeMode="contain"
              style={styles.heart}
              source={require('../../assets/emptyheart.png')}
            />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={styles.imageContainer}
            onPress={() => {
              removeFromFav({
                name: itemData.item.name,
              });
            }}>
            <Image
              resizeMode="contain"
              style={styles.heart}
              source={require('../../assets/heart.png')}
            />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

//Utilizo StyleSheet en vez de CSS para evitar que
//el style pase por el bridge cada render
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderColor: 'gray',
    borderWidth: 1,
    width: '80%',
    flex: 1,
    padding: '2%',
    margin: '2%',
    alignSelf: 'center',
    shadowColor: 'gray',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 1,
    borderColor: Colors.pink,
    borderWidth: 2,
    borderRadius: 1,
    flexWrap: 'wrap',
  },
  heart: {width: 30, height: 30},
  imageContainer: {
    flex: 1,
    marginRight: '1%',
    alignItems: 'flex-end',
  },
  text: {marginLeft: '2%', fontSize: 24, flexWrap: 'wrap'},
});
