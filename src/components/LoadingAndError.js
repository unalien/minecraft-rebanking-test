import {StyleSheet, Image, Text, View} from 'react-native';
import React from 'react';
export default LoadingAndError = props => (
  <View style={styles.container}>
    <Image
      resizeMode="contain"
      style={styles.image}
      source={require('../../assets/llamaconropa.png')}
    />
    <Text style={styles.text}>{props.message}</Text>
  </View>
);
const styles = StyleSheet.create({
  text: {fontSize: 25, color: Colors.pink, fontWeight: 'bold'},
  image: {width: '50%', height: '50%'},
  container: {flex: 1, alignItems: 'center', justifyContent: 'center'},
});
