import React from 'react';
import {View, TouchableOpacity, StyleSheet, Image, Text} from 'react-native';
import Colors from '../utils/colors';
const Card = props => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.touch}
        onPress={() => props.navigation.navigate(props.screenToNavigate)}>
        <Image style={styles.image} source={props.imageSrc} />
        <Text style={styles.text}>{props.title}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {fontSize: 20},
  touch: {flexDirection: 'row', alignItems: 'center'},
  container: {
    shadowColor: 'gray',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 1,
    borderColor: Colors.pink,
    borderWidth: 2,
    borderRadius: 1,
    width: '80%',
    margin: '5%',
  },
  image: {
    margin: '5%',
    height: 80,
    width: 80,
  },
});

export default Card;
