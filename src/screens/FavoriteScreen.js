import React, {Component} from 'react';
import FavoriteContainer from '../containers/FavoriteContainer';

class FavoriteScreen extends Component {
  render() {
    return <FavoriteContainer />;
  }
}

export default FavoriteScreen;
