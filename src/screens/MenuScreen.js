import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Card from '../components/Cards';
import Colors from '../utils/colors';
class MenuScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Navega y dale like!</Text>
        <Card
          navigation={this.props.navigation}
          screenToNavigate="Items"
          imageSrc={require('../../assets/grass.png')}
          title="BLOQUES"
        />
        <Card
          navigation={this.props.navigation}
          screenToNavigate="Ent"
          imageSrc={require('../../assets/sheep.png')}
          title="COSAS"
        />
        <Card
          navigation={this.props.navigation}
          screenToNavigate="Fav"
          imageSrc={require('../../assets/heart.png')}
          title="FAVORITOS"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 25,
    color: Colors.pink,
    marginBottom: '5%',
    fontWeight: 'bold',
  },
});
export default MenuScreen;
