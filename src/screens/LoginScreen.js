import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Image,
  TouchableWithoutFeedback,
  Platform,
  Keyboard,
  Button,
  KeyboardAvoidingView,
  StatusBar,
  Text,
} from 'react-native';
import Colors from '../utils/colors';
import login from '../data/login';
import {StackActions, NavigationActions} from 'react-navigation';

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'Main'})],
});

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.user = '';
    this.password = '';
  }
  state = {mensaje: ''};
  loginM() {
    if (this.user === '' || this.password === '') {
      this.setState({
        mensaje: 'Debe ingresar usuario y password para continuar',
      });
      return;
    } else {
      this.setState({mensaje: ''});
    }
    const loginSuc = login(this.user, this.password);
    if (loginSuc) {
      this.props.navigation.dispatch(resetAction);
    }
  }
  render() {
    return (
      <TouchableWithoutFeedback
        onPress={Keyboard.dismiss}
        style={{
          paddingTop: Platform.OS === 'ios' ? StatusBar.currentHeight : 0,
        }}>
        <View style={styles.container}>
          <View style={styles.imageBackground}>
            <Image
              style={styles.image}
              source={require('../../assets/personajes.png')}
            />
          </View>
          <KeyboardAvoidingView
            behavior="padding"
            enabled
            style={styles.inputContainer}>
            <Text>{this.state.mensaje}</Text>
            <TextInput
              style={styles.input}
              onChangeText={text => {
                this.user = text;
              }}
              placeholder="User"
            />
            <TextInput
              style={styles.input}
              editable={true}
              onChangeText={text => {
                this.password = text;
              }}
              placeholder="Password"
            />

            <Button
              borderRadius={20}
              onPress={() => this.loginM()}
              title="Entrar"
              color="purple"
              fontSize="16"
            />
          </KeyboardAvoidingView>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.pink,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    height: 30,
    borderRadius: 20,
    paddingHorizontal: '5%',
    backgroundColor: 'black',
    borderBottomColor: '#D8DEE1',
    borderBottomWidth: 1,
    width: '100%',
    margin: '2%',
    padding: 0,
    backgroundColor: 'white',
  },
  button: {
    borderRadius: 20,
    width: '80%',
  },
  inputContainer: {
    width: '80%',
    flex: 1,
    margin: '10%',
  },
  image: {
    resizeMode: 'cover',
    width: 300,
    height: 300,
  },
  imageBackground: {flex: 3, justifyContent: 'flex-end'},
});

export default LoginScreen;
